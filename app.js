var app;
var rootElement = document.getElementById("main");
const canvasHeight = Math.round(innerHeight / 2);//window.innerHeight / 3;
const canvasWidth = innerWidth;//window.innerWidth / 2;

fetch('/chart_data.json')
    .then(result => result.json())
    .then((result) => {
        console.log(result);
        app = new App(result);
        app.drawCtx(rootElement);
    })

class Column {
    constructor(_id) {
        this.id = _id;
        this.data_points = [];
        this.color = "";
        this.name = "";
        this.type = "";

        this.max = 0;
        this.min = 0;
    }
}

class Chart {
    constructor(_chart, i) {
        this.maxX = 0;
        this.minX = 0;
        this.maxY = 0;
        this.minY = 0;

        this.columns = new Array();
        _chart.columns.forEach((column, i) => {
            this.columns.push(new Column(column.shift()));
            this.columns[i].data_points = column;
        });
        for (let [key, value] of Object.entries(_chart.colors)) {
            this.columns.find((el, i) => {
                if (el.id === key) {
                    this.columns[i].color = value;
                }
            });
        }
        for (let [key, value] of Object.entries(_chart.names)) {
            this.columns.find((el, i) => {
                if (el.id === key) {
                    this.columns[i].name = value;
                }
            });
        }
        for (let [key, value] of Object.entries(_chart.types)) {
            this.columns.find((el, i) => {
                if (el.id === key) {
                    this.columns[i].type = value;
                }
            });
        }

        this.columns.find((el, i) => {
            el.max = Math.max.apply(null, el.data_points);
            el.min = Math.min.apply(null, el.data_points);
        });

        this.canvas = document.createElement("canvas");
        this.canvas.id = i;
        this.canvas.height = canvasHeight;
        this.canvas.width = canvasWidth;
        this.ctx = this.canvas.getContext("2d");
        this.ctx.strokeRect(0, 0, canvasWidth, canvasHeight);

        this.drawGrid = () => {
            this.canvas.height =  Math.round(innerHeight / 2);
            this.canvas.width = innerWidth;
            this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

            this.columns.find((el, i) => {
                if (el.id === 'x') {
                    let kX = (this.ctx.canvas.width) / (el.max - el.min);
                    // let mX = this.ctx.canvas.width;
                    this.ctx.fillStyle = "#00000099";
                    let prevPoint = 0;
                    el.data_points.forEach((elc, i) => {
                        if (Math.abs(((elc - el.min) * kX) - ((el.data_points[prevPoint] - el.min) * kX)) > this.ctx.canvas.width * 0.12) {
                            console.log((elc - el.min) * kX, ((el.data_points[prevPoint] - el.min) * kX), Math.abs(((elc - el.min) * kX) - ((el.data_points[prevPoint] - el.min) * kX)) > this.ctx.canvas.width * 0.12)
                            prevPoint = i;
                            this.ctx.fillRect((elc - el.min) * kX, 5, 1, this.canvas.height - 10);
                        }
                    });
                    // this.ctx.strokeStyle = "#000000";
                    // this.ctx.lineWidth = 1;
                    // this.ctx.strokeRect(0, 0, el.max * kX, canvasHeight);
                    // this.ctx.stroke();
                    // console.log(kX, kX / el.min, kX / el.max, this.ctx.canvas.width);
                }
            });
        }
    }
}

class App {
    constructor(_chart_data) {
        this.chart = new Array();
        this.chart_data = _chart_data;
        this.chart_count = _chart_data.length;
        _chart_data.forEach((chart, i) => {
            this.chart.push(new Chart(chart, i));
        });

        this.refreshGrid = () => {
            this.chart.forEach((el, i) => {
                el.drawGrid();
            });
        }

        window.addEventListener("resize", this.refreshGrid);

        this.drawCtx = (root) => {
            this.chart.forEach((el, i) => {
                root.appendChild(el.canvas);
                el.drawGrid();
            });
        }
    }


}



