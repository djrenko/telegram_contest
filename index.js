var http = require('http');
var fs = require('fs');
// var index = fs.readFileSync('index.html');
// var app = fs.readFileSync('app.js');
// var chart_data = fs.readFileSync('chart_data.json');
var port = 9600;


http.createServer(function (req, res) {

    var index = fs.readFileSync('index.html');
    var app = fs.readFileSync('app.js');
    var chart_data = fs.readFileSync('chart_data.json');

    if (req.url === '/') {
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
        res.end(index);
    } else
    if (req.url === '/app.js') {
        res.writeHead(200, {
            'Content-Type': 'text/javascript'
        });
        res.end(app);
    } else
    if (req.url === '/chart_data.json') {
        res.writeHead(200, {
            'Content-Type': 'text/javascript'
        });
        res.end(chart_data);
    } else {
        res.statusCode = 404;
        res.end(null);
    }
}).listen(port).on("listening", (e) => {
    console.log('Server work on ' + port + ' port...')
});